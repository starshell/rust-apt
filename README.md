# rust-apt

A rust wrapper around the `apt` package manager commands.

## Tests

Tests must be ran with only one thread since `apt` has no threading model.

    cargo test -- --test-threads=1
