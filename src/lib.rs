use std::process::{Command, ExitStatus, Stdio};

pub fn exists() -> bool {
    let get = match Command::new("which")
        .arg("apt-get") 
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
    {
        Ok(status) => status.success(),
        Err(_) => false,
    };

    let cache = match Command::new("which")
        .arg("apt-cache")
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
    {
        Ok(status) => status.success(),
        Err(_) => false,
    };

    let dpkg = match Command::new("which")
        .arg("dpkg")
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
    {
        Ok(status) => status.success(),
        Err(_) => false,
    };

    get && cache && dpkg
}

pub fn update() -> Result<ExitStatus, std::io::Error> {
    Command::new("apt-get")
        .arg("update")
        .arg("--assume-yes")
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
}

pub fn install(package: &str) -> Result<ExitStatus, std::io::Error> {
    Command::new("apt-get")
        .arg("install")
        .arg("--assume-yes")
        .arg(package)
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
}

pub fn remove(package: &str) -> Result<ExitStatus, std::io::Error> {
    Command::new("apt-get")
        .arg("remove")
        .arg("--assume-yes")
        .arg(package)
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_exists() {
        assert_eq!(true, exists());
    }

    #[test]
    fn test_update() {
        let status = update().unwrap();
        let success = status.success();
        let code = status.code();
        assert_eq!(true, success);
        assert_eq!(Some(0), code);
    }

    #[test]
    fn test_install() {
        remove("fortunes");
        let status = install("fortunes").unwrap();
        let success = status.success();
        let code = status.code();
        assert_eq!(true, success);
        assert_eq!(Some(0), code);
    }

    #[test]
    fn test_install_failure() {
        let status = install("abc123").unwrap();
        let success = status.success();
        let code = status.code();
        assert_eq!(false, success);
        assert_eq!(Some(100), code);
    }

    #[test]
    fn test_remove() {
        install("fortunes");
        let status = remove("fortunes").unwrap();
        let success = status.success();
        let code = status.code();
        assert_eq!(true, success);
        assert_eq!(Some(0), code);
    }

    #[test]
    fn test_remove_failure() {
        let status = remove("abc123").unwrap();
        let success = status.success();
        let code = status.code();
        assert_eq!(false, success);
        assert_eq!(Some(100), code);
    }
}
